# Circle + Segment for Android [![](https://jitpack.io/v/com.gitlab.massarttech/android-segment-progress.svg)](https://jitpack.io/#com.gitlab.massarttech/android-segment-progress)


**Circle and segment progress bar**
<br/>
<img src="https://gitlab.com/massarttech/android-segment-progress/raw/master/screen/screen1.png?inline=false" width="300" height="500"/>


## How To Use

### Setup
Add it to your build.gradle with:
```gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
and:

```gradle
dependencies {
     implementation 'com.gitlab.massarttech:android-segment-progress:1.0.0'
}
```


### Usage
#### Add Circle Progress bar 
```xml
<com.massarttech.android.progress.AdCircleProgress
        android:layout_gravity="center_horizontal"
        android:id="@+id/progressBar"
        android:layout_width="250dp"
        app:adpgb_background_color="@android:color/transparent"
        android:layout_margin="20dp"
        android:layout_height="250dp"
        app:adpgb_finished_color="@color/colorPrimary"
        app:adpgb_unfinished_color="@android:color/transparent"
        app:adpgb_finished_stroke_width="40dp"
        app:adpgb_progress="10"
        app:adpgb_max="100"
        app:adpgb_text_size="20sp"
        app:adpgb_show_text="false"
        app:adpgb_unfinished_stroke_width="5dp"/>
    
```
#### Add Segment Progress bar
```xml
 <com.massarttech.android.progress.SegmentedProgressBar
        app:fill_color="@color/colorAccent"
        android:layout_margin="10dp"
        app:segment_count="4"
        android:id="@+id/segmentBar"
        android:layout_width="match_parent"
        android:layout_height="5dp"/>
```