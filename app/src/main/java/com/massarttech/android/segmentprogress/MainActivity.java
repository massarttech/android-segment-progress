package com.massarttech.android.segmentprogress;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.massarttech.android.progress.AdCircleProgress;
import com.massarttech.android.progress.SegmentedProgressBar;

import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final SegmentedProgressBar segmentedProgressBar = findViewById(R.id.segmentBar);
        final AdCircleProgress     circleProgress       = findViewById(R.id.progressBar);
        ScheduledExecutorService   service              = Executors.newSingleThreadScheduledExecutor();
        final AtomicInteger        integer              = new AtomicInteger(10);
        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                int value = integer.get();
                if (value > circleProgress.getMax()) {
                    integer.set(10);
                } else {
                    integer.set(value + 10);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        circleProgress.setAdProgress(integer.get());
                        segmentedProgressBar.playSegment(200);
                    }
                });
            }
        }, 10, 300, TimeUnit.MILLISECONDS);
    }
}
